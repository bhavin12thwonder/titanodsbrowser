﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using org.asam.ods;

namespace TitanOdsBrowser.Services.Interfaces
{
    public interface IConnectionService
    {
        AoSession GetConnection(string user);
    }
}