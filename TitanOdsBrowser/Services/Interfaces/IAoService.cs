﻿using org.asam.ods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TitanOdsBrowser.Services.Interfaces
{
    public interface IAoService
    {
        JsonResult GetChildren(AoSession aoSession, string child, string parent, string parentId);
    }
}